package zfbservice

import (
	"context"
	"fmt"
	"log"
	"regexp"
	"strings"
	"sync"
	"time"

	"github.com/chromedp/cdproto/network"
	"github.com/chromedp/cdproto/target"
	"github.com/chromedp/chromedp"
	"github.com/gogf/gf/encoding/gbase64"
	"github.com/gogf/gf/frame/g"
	"github.com/gogf/gf/net/ghttp"
	"github.com/gogf/gf/os/glog"
	"github.com/gogf/gf/util/gconv"
	"github.com/gogf/guuid"
)

var (
	Browser = NewBrowser()
)

func NewBrowser() (b *GBrowser) {
	b = &GBrowser{}
	b.items = map[BindID]*BrowserInfo{}
	b.CloseWindow = true // true 关闭,  false 打开

	b.InitBrowser()

	go func() {
		for {

			ids := []string{}
			for k, _ := range b.GetBrowserInfoAll() {
				ids = append(ids, k)
			}

			fmt.Printf("\n----------------------------------\n"+
				"内核数量: %+v\n"+
				"在线ID: \n%+v\n"+
				"----------------------------------\n",
				b.GetBrowserInfoLen(),
				strings.Join(ids, "\n"),
			)

			time.Sleep(time.Second * 10)
		}

	}()
	return
}

type (
	BindID = string // 浏览器绑定ID,默认绑定ID为: "default"

	BrowserInfo struct {
		Ctx          context.Context
		Cancel       context.CancelFunc
		TargetId     target.ID
		Uid          string
		Token        string
		Cookie       []*network.Cookie
		IsLuck       bool // 如果为 true 是锁住状态, false 是解锁状态
		FengKong     int64
		TimeOutCount int64
	}

	GBrowser struct {
		sync.Mutex
		items       map[BindID]*BrowserInfo
		Default     *BrowserInfo
		CloseWindow bool
	}
)

func (b *GBrowser) InitBrowser() {
	b.Default = &BrowserInfo{}
	ctxSet, _ := chromedp.NewExecAllocator(
		context.Background(),

		// 以默认配置的数组为基础，覆写headless参数
		// 当然也可以根据自己的需要进行修改，这个flag是浏览器的设置
		append(
			chromedp.DefaultExecAllocatorOptions[:],
			chromedp.Flag("headless", b.CloseWindow), // true 关闭,  false 打开
			// chromedp.Flag("useAutomationExtension", true),
			// chromedp.Flag("excludeSwitches", []string{"enable-automation"}),
		)...,
	)

	b.Default.Ctx, b.Default.Cancel = chromedp.NewContext(
		// context.Background(),
		ctxSet,
		// chromedp.WithDebugf(log.Printf),
	)

}
func (b *GBrowser) CreateBrowser() (bid BindID) {
	bid = gconv.String(guuid.New())

	bi := &BrowserInfo{}
	ctxSet, _ := chromedp.NewExecAllocator(
		context.Background(),

		// 以默认配置的数组为基础，覆写headless参数
		// 当然也可以根据自己的需要进行修改，这个flag是浏览器的设置
		append(
			chromedp.DefaultExecAllocatorOptions[:],
			chromedp.Flag("headless", b.CloseWindow), // true 关闭,  false 打开
			// chromedp.Flag("useAutomationExtension", false),
			// chromedp.Flag("excludeSwitches", []string{"enable-automation"}),
		)...,
	)

	bi.Ctx, bi.Cancel = chromedp.NewContext(
		// context.Background(),
		ctxSet,
		// chromedp.WithDebugf(log.Printf),
	)

	b.Lock()
	b.items[bid] = bi
	b.Unlock()

	return
}
func (b *GBrowser) AddBrowserInfo(bid BindID, bi *BrowserInfo) {
	b.Lock()
	defer b.Unlock()
	b.items[bid] = bi
	return
}
func (b *GBrowser) GetBrowserInfo(bid BindID) (bi *BrowserInfo, ok bool) {
	b.Lock()
	defer b.Unlock()
	bi, ok = b.items[bid]
	return
}
func (b *GBrowser) GetBrowserInfoLen() (l int) {
	b.Lock()
	defer b.Unlock()
	l = len(b.items)
	return
}
func (b *GBrowser) GetBrowserInfoAll() map[BindID]*BrowserInfo {
	b.Lock()
	defer b.Unlock()
	return b.items
}
func (b *GBrowser) DelBrowserInfo(bid BindID) {
	b.Lock()
	defer b.Unlock()
	delete(b.items, bid)
	return
}

func NewTradeOrderService() (t *tradeOrderService) {
	t = &tradeOrderService{}
	return
}

type tradeOrderService struct{}

type LoginChan struct {
	Ctx context.Context
	Bid BindID
	// Token string
	// Pid string
}

type DelLoginUser struct {
	Pid string `json:"pid"`
}

type GetLoginQrcode struct {
	CallBackUrl string `json:"call_back_url"`
}

type GetLoginQrcodeRes struct {
	CodeImgBase64 string `json:"code_img_base_64"`
}

func (t *tradeOrderService) DelLoginUser(r *ghttp.Request, param *DelLoginUser) (err error) {
	bi, ok := Browser.GetBrowserInfo(param.Pid)
	if ok {
		bi.Cancel()
		Browser.DelBrowserInfo(param.Pid)
		glog.Debugf("删除 ===>>>> %v", param.Pid)

	} else {
		glog.Warningf("删除的ID[%v]不存在.", param.Pid)
	}

	return
}

// GetLoginQrcode 获取登录二维码
func (t *tradeOrderService) GetLoginQrcode(r *ghttp.Request, param *GetLoginQrcode) (rs *GetLoginQrcodeRes, err error) {
	rs = &GetLoginQrcodeRes{}
	lc := make(chan LoginChan, 1024)
	lc2 := make(chan LoginChan, 1024)

	// downloadURL := "https://auth.alipay.com/login/index.htm?goto=https://my.alipay.com/portal/i.htm"
	downloadURL := `https://auth.alipay.com/login/index.htm?goto=https%3A%2F%2Fwww.alipay.com%2F`
	// downloadURL := `https://auth.alipay.com/login/index.htm?goto=https://consumeprod.alipay.com/record/advanced.htm`
	// https://consumeprod.alipay.com/record/advanced.htm

	bid := Browser.CreateBrowser()
	bi, ok := Browser.GetBrowserInfo(bid)
	if !ok {
		glog.Errorf("浏览器未找到...")
		Browser.DelBrowserInfo(bid)
		return
	}

	type ReqInfo struct {
		ReqType   string
		RequestID network.RequestID
		Request   *network.Request
	}

	var (
		LoginOKInfo   = &ReqInfo{}
		JumpTreadInfo = &ReqInfo{}
		FengKongInfo  = &ReqInfo{}
	)
	go func() {
		chromedp.ListenBrowser(bi.Ctx, func(v interface{}) {
			switch ev := v.(type) {

			case *target.EventTargetCreated: // 创建浏览器
				// g.Dump("创建浏览器事件>>>",ev)
				if ev.TargetInfo.Type == "page" && ev.TargetInfo.URL == "about:blank" {
					bi.TargetId = ev.TargetInfo.TargetID
				}

			case *target.EventTargetDestroyed: // 浏览器关闭
				// g.Dump("浏览器关闭事件>>>",ev)
				if ev.TargetID == bi.TargetId {
					fmt.Printf("浏览器被关闭 >> 支付宝ID: %+v\n", bi.Uid)

					bi.Cancel()
					Browser.DelBrowserInfo(bi.Uid)
				}
			case *target.EventTargetInfoChanged:
				g.Dump("====页面跳转=>>", ev.TargetInfo.URL)

				if strings.Contains(ev.TargetInfo.URL, `https://consumeprod.alipay.com/record/standard.htm`) {
					// https://consumeprod.alipay.com:443/record/switchVersion.htm

					go func() {
						g.Dump("====跳转到高级版=>>", ev)
						if err := chromedp.Run(bi.Ctx, chromedp.Navigate(`https://consumeprod.alipay.com:443/record/switchVersion.htm`)); err != nil {
							glog.Errorf("==跳转>>>出错了: %+v", err)
						}
					}()
				}

			default:
				// g.Dump(reflect.TypeOf(ev))
			}
		})

		chromedp.ListenTarget(bi.Ctx, func(v interface{}) {
			switch ev := v.(type) {

			case *network.EventRequestWillBeSent: // 请求事件
				// 获取二维码图片
				switch {
				case strings.Contains(ev.Request.URL, `https://consumeprod.alipay.com/record/checkSecurity.htm`):
					// g.Dump("风控二维码....????")

					FengKongInfo.RequestID = ev.RequestID
					FengKongInfo.Request = ev.Request

					break
				case strings.Contains(ev.Request.URL, `https://www.alipay.com`):
					// g.Dump("\n登录成功>>>",ev.RequestID)

					LoginOKInfo.RequestID = ev.RequestID
					LoginOKInfo.Request = ev.Request

					break
				case strings.Contains(ev.Request.URL, `https://consumeprod.alipay.com/record/advanced.htm`):
					// g.Dump("跳转交易中心>>>",ev.RequestID)
					JumpTreadInfo.RequestID = ev.RequestID
					JumpTreadInfo.Request = ev.Request

					break

				}

			case *network.EventLoadingFinished: // 加载完成
				switch ev.RequestID {
				case LoginOKInfo.RequestID:
					// https://authstl.alipay.com/login/loginResultDispatch.htm
					// if LoginOKInfo.Request.Headers["Referer"] == "https://authstl.alipay.com/login/loginResultDispatch.htm" {
					//
					// }

					fmt.Printf("获取登录信息>>>>%+v, %+v\n", ev.RequestID, LoginOKInfo.Request.URL)
					// g.Dump(LoginOKInfo.Request)

					g.Dump("====>>>", LoginOKInfo.Request, strings.Contains(gconv.String(LoginOKInfo.Request.Headers["Referer"]), `https://auth`))

					if strings.Contains(gconv.String(LoginOKInfo.Request.Headers["Referer"]), `://auth`) {
						go func() {
							if err := chromedp.Run(bi.Ctx, chromedp.ActionFunc(func(ctx context.Context) error {

								cookieSlice, err := network.GetAllCookies().Do(ctx)
								if err != nil {
									g.Dump("Cookie获取错误:", err)
								}
								bi.Cookie = cookieSlice
								// g.Dump("===cookie==>>",cookieSlice)
								for _, v := range cookieSlice {
									if v.Name == "ctoken" {
										bi.Token = v.Value
									}

									if v.Name == "ali_apache_tracktmp" {
										v.Value = strings.Replace(v.Value, `"`, ``, -1)
										tmpSlice := strings.Split(v.Value, `=`)
										if len(tmpSlice) == 2 {
											bi.Uid = tmpSlice[1]

											if _, ok := Browser.GetBrowserInfo(bi.Uid); !ok {
												// fmt.Printf("===所有信息===>>>%+v\n", Browser.GetBrowserInfoAll())
												Browser.AddBrowserInfo(bi.Uid, bi)
												Browser.DelBrowserInfo(bid)
												bid = bi.Uid
												// fmt.Printf("===bi===>>>%+v\n",bi)
												// fmt.Printf("===所有信息2===>>>%+v\n", Browser.GetBrowserInfoAll())
											}

											lc <- LoginChan{
												Ctx: bi.Ctx,
												Bid: bi.Uid,
											}
										}

									}
								}

								return err
							})); err != nil {
								glog.Errorf("==>>>出错了: %+v", err)
							}
						}()

					} else {

						// 账号被风控
						if bi.FengKong > 5 {
							fmt.Printf("账号[%+v]被风控 ==>> %+v\n", bi.Uid, bi.FengKong)

							// 调用回调接口
							go func() {

								rsData := CallBackInfo{}
								rsData.Code = 400
								rsData.Message = "账号被风控"
								rsData.DataType = "AccountRisk"
								rsData.Data = ""

								if r, err := g.Client().Post(param.CallBackUrl, rsData); err != nil {
									glog.Errorf("风控回调接口访问错误: %+v", err)
								} else {
									defer r.Close()
									g.Dump("风控回调完成 >>>> ", r.ReadAllString())
								}

								bi.Cancel()
								Browser.DelBrowserInfo(bi.Uid)

							}()

						}

						bi.FengKong++

						fmt.Printf("[%+v]累计风控数量 ==>> %+v\n", bi.Uid, bi.FengKong)
					}

					break
				case FengKongInfo.RequestID:

					g.Dump("风控ID已完成....")

					if !bi.IsLuck {
						bi.IsLuck = true // 锁住
					}

					go func() {

						// 截取图片by 元素
						var buf []byte

						if err := chromedp.Run(bi.Ctx, chromedp.ActionFunc(func(ctx context.Context) (err error) {
							sel := `#risk_qrcode_cnt>canvas`
							if err = chromedp.WaitVisible(sel, chromedp.ByID).Do(ctx); err != nil {
								glog.Errorf("等待元素存在时出错: %+v", err)
								return
							}

							if err = chromedp.Screenshot(sel, &buf, chromedp.NodeVisible, chromedp.ByID).Do(ctx); err != nil {
								glog.Errorf("截取图片时出错: %+v", err)
								return
							}

							return
						})); err != nil {
							glog.Errorf("==>>>出错了: %+v", err)
							return
						}

						imgBase64 := gbase64.EncodeToString(buf)

						// g.Dump(imgBase64)

						glog.Warningf("二次登录二维码已经获取  >> 长度: %v\n", len(imgBase64))
						// 需要二次登录
						go func() {

							imgCode := GetLoginQrcodeRes{CodeImgBase64: imgBase64}
							rsData := CallBackInfo{}
							rsData.Code = 200
							rsData.Message = "请求二次登录"
							rsData.DataType = "LoginTwoCode"
							rsData.Data = imgCode

							if r, err := g.Client().Post(param.CallBackUrl, rsData); err != nil {
								glog.Errorf("请求二次登录回调接口访问错误: %+v, %+v, %+v\n", len(gconv.String(imgCode)), err, param.CallBackUrl)
							} else {
								defer r.Close()
								fmt.Printf("请求二次登录成功回调成功>>> %+v, %+v, %+v\n", len(gconv.String(imgCode)), r.ReadAllString(), param.CallBackUrl)
							}
						}()

						for {
							select {
							case <-time.After(time.Second * 60):

								g.Dump("二次登录60秒超时...")

								if bi.TimeOutCount < 10 {
									fmt.Printf("二次登录超时重新发送二维码...[%+v]超时次数 %+v\n", bi.Uid, bi.TimeOutCount)
									// 需要二次登录
									go func() {

										imgCode := GetLoginQrcodeRes{CodeImgBase64: imgBase64}
										rsData := CallBackInfo{}
										rsData.Code = 200
										rsData.Message = "请求二次登录"
										rsData.DataType = "LoginTwoCode"
										rsData.Data = imgCode

										if r, err := g.Client().Post(param.CallBackUrl, rsData); err != nil {
											glog.Errorf("请求二次登录回调接口访问错误: %+v, %+v, %+v\n", len(gconv.String(imgCode)), err, param.CallBackUrl)
										} else {
											defer r.Close()
											fmt.Printf("请求二次登录成功回调成功>>> %+v, %+v, %+v\n", len(gconv.String(imgCode)), r.ReadAllString(), param.CallBackUrl)
										}
									}()

									go func() {
										g.Dump("====跳转到二次登录风控二维码获取页面=>>", ev)
										if err := chromedp.Run(bi.Ctx, chromedp.Navigate(`https://consumeprod.alipay.com/record/advanced.htm`)); err != nil {
											glog.Errorf("==跳转到二次登录风控二维码获取页面>>>出错了: %+v", err)
										}
									}()

									bi.TimeOutCount++

								} else {
									bi.TimeOutCount = 0
									bi.Cancel()
									Browser.DelBrowserInfo(bi.Uid)

									rsData := CallBackInfo{}
									rsData.Code = 400
									rsData.Message = "二次登录超时"
									rsData.DataType = "LoginTwo"
									if r, err := g.Client().Post(param.CallBackUrl, rsData); err != nil {
										glog.Errorf("二次登录回调接口访问错误: %+v,%+v", err, param.CallBackUrl)
									} else {
										defer r.Close()
										g.Dump("二次登录错误返回的回调成功 >>>> ", r.ReadAllString())
									}
								}

								return
							case data := <-lc2:
								fmt.Printf("二次登录登录成功===>>>>: %+v\n", data.Bid)

								bi.IsLuck = false // 解锁

								// 调用回调接口
								go func() {

									rsData := CallBackInfo{}
									rsData.Code = 200
									rsData.Message = "二次登录成功"
									rsData.DataType = "LoginTwo"

									if r, err := g.Client().Post(param.CallBackUrl, rsData); err != nil {
										glog.Errorf("二次登录回调接口访问错误: %+v", err)
									} else {
										defer r.Close()
										g.Dump("二次登录成功回调完成 >>>> ", r.ReadAllString())
									}
								}()

								// if err := chromedp.Run(bi.Ctx,chromedp.Navigate(`https://consumeprod.alipay.com/record/advanced.htm`)); err != nil {
								// 	glog.Errorf("==跳转>>>出错了: %+v",err)
								// }

								return
							}
						}

					}()

					break
				case JumpTreadInfo.RequestID:
					// 二次登录成功
					// https://consumeprod.alipay.com/record/checkSecurity.htm?consumeVersion=advanced&securityId=web%7Cconsumeprod_record_list%7Ce0c3cd58-0cc3-40ea-bb22-9116e8fc11b8GZ00
					if strings.Contains(gconv.String(JumpTreadInfo.Request.Headers["Referer"]), `https://consumeprod.alipay.com/record/checkSecurity.htm?consumeVersion=`) {

						lc2 <- LoginChan{
							Ctx: bi.Ctx,
							Bid: bi.Uid,
						}

					} else {
						go doGetListData(bi.Uid, param.CallBackUrl, ev.RequestID)
					}

					break
				}

			}
		})
	}()

	// 截取图片by 元素
	var buf []byte
	if err := chromedp.Run(bi.Ctx, elementScreenshot(downloadURL, `J-barcode-container`, &buf)); err != nil {
		log.Fatal(err)
	}

	imgBase64 := gbase64.EncodeToString(buf)

	// g.Dump(imgBase64)
	glog.Warningf("二维码已经获取 >> 长度: %v\n", len(imgBase64))
	rs.CodeImgBase64 = imgBase64

	// 检测登录
	go checkLogin(bid, lc, param.CallBackUrl)

	return
}

// 截取元素 图片
func elementScreenshot(urlstr, sel string, res *[]byte) chromedp.Tasks {
	return chromedp.Tasks{
		chromedp.Navigate(urlstr),
		chromedp.WaitVisible(sel, chromedp.ByID),
		chromedp.Screenshot(sel, res, chromedp.NodeVisible, chromedp.ByID),
	}
}

type CallBackInfo struct {
	Code     int64       `json:"code"`
	DataType string      `json:"data_type"`
	Message  string      `json:"message"`
	Data     interface{} `json:"data"`
}

type LoginRs struct {
	Mid string `json:"mid"`
	Pid string `json:"pid"`
}

func checkLogin(bid BindID, lc chan LoginChan, callBackUrl string) {
	bi, ok := Browser.GetBrowserInfo(bid)
	if !ok {
		glog.Errorf("[%v]浏览器未找到...", bid)
		Browser.DelBrowserInfo(bid)
		return
	}
	for {
		select {
		case <-time.After(time.Second * 60):

			bi.Cancel()
			Browser.DelBrowserInfo(bid)

			g.Dump("登录超时...")
			rsData := CallBackInfo{}
			rsData.Code = 400
			rsData.Message = "登录超时"
			rsData.DataType = "Login"
			rd := LoginRs{}
			rsData.Data = rd
			if r, err := g.Client().Post(callBackUrl, rsData); err != nil {
				glog.Errorf("回调接口访问错误: %+v,%+v", err, callBackUrl)
			} else {
				defer r.Close()
				g.Dump("回调成功 >>>> ", r.ReadAllString())
			}

			return
		case data := <-lc:
			fmt.Printf("登录成功===>>>>: %+v\n", data.Bid)

			// 每隔5秒跳转一次
			go func() {

				for {
					select {
					case <-bi.Ctx.Done():
						return
					default:

						fmt.Printf("[%+v] 是否停止: %+v\n", bi.Uid, bi.IsLuck)

						if !bi.IsLuck {
							if err := chromedp.Run(data.Ctx, chromedp.Navigate(`https://consumeprod.alipay.com/record/advanced.htm`)); err != nil {
								glog.Errorf("==跳转>>>出错了: %+v", err)
							}
						}
					}
					fmt.Printf("开始循环跳转 >>> \n")
					time.Sleep(time.Second * 5)

				}
			}()

			// 调用回调接口
			go func() {

				rsData := CallBackInfo{}
				rsData.Code = 200
				rsData.Message = "登录成功"
				rsData.DataType = "Login"
				rd := LoginRs{
					Pid: data.Bid,
				}
				rsData.Data = rd

				if r, err := g.Client().Post(callBackUrl, rsData); err != nil {
					glog.Errorf("回调接口访问错误: %+v", err)
				} else {
					defer r.Close()
					g.Dump("登录成功回调完成 >>>> ", r.ReadAllString())
				}

			}()

			fmt.Printf("===所有信息===>>>%+v\n", Browser.GetBrowserInfoAll())

			return
		}
	}
}

func doGetListData(bid BindID, callBackUrl string, rid network.RequestID) {

	bi, ok := Browser.GetBrowserInfo(bid)
	if !ok {
		glog.Errorf("[%v]浏览器未找到...", bid)
		Browser.DelBrowserInfo(bid)
		return
	}
	if err := chromedp.Run(bi.Ctx, chromedp.ActionFunc(func(ctx context.Context) (err error) {
		html, err := network.GetResponseBody(rid).Do(ctx)
		rs := formatData(gconv.String(html))

		if len(rs) <= 0 {
			glog.Warningf("[%v]获取数据失败>>>  %+v条数据\n", bi.Uid, len(rs))
			return
		}

		// 调用回调接口
		rsData := CallBackInfo{}
		rsData.Code = 200
		rsData.Message = "数据获取成功"
		rsData.DataType = "DataList"

		rsData.Data = rs

		if r, err := g.Client().Post(callBackUrl, rsData); err != nil {
			glog.Errorf("获取数据回调接口访问错误: %+v", err)
		} else {
			defer r.Close()
			// g.Dump("获取数据回调接口成功 >>>> ",r.ReadAllString())
		}

		fmt.Printf("[%v]数据获取成功>>>  %+v条数据\n", bi.Uid, len(rs))

		return
	})); err != nil {
		glog.Errorf("==>>>出错了: %+v", err)
	}

}

type RsDataInfo struct {
	Name   string `json:"name"`
	Price  string `json:"price"`
	Status string `json:"status"`
	Ts     string `json:"ts"`
}

type RsList []RsDataInfo

// 格式化数据
func formatData(html string) (rl RsList) {
	// 创建时间    名称    金额  状态
	rl = RsList{}

	reg := regexp.MustCompile(`(?Us:<tr id="J-item-.*>(.*)</tr>)`)
	reList := reg.FindAllStringSubmatch(html, -1)
	fmt.Printf("正则信息 ==>> html长度:%+v,tr长度:%+v,\n", len(html), len(reList))

	for _, v := range reList {

		tmpHtml := v[0]

		reg1 := regexp.MustCompile(`(?Us:<td class="time">(.*)</td>)`)
		reg_time := reg1.FindStringSubmatch(tmpHtml)
		rs_time := strings.Replace(reg_time[0], `.`, "-", -1)
		rs_time = trimHtml(rs_time) + ":00"

		reg2 := regexp.MustCompile(`(?Us:<td class="name">(.*)</td>)`)
		reg_name := reg2.FindStringSubmatch(tmpHtml)
		if len(reg_name) <= 0 || reg_name[0] == "" {
			glog.Errorf("正则数据[reg_name]为空 %+v", reg_name)
			return
		}
		rs_name := trimHtml(reg_name[0])

		reg3 := regexp.MustCompile(`(?Us:<td class="amount">(.*)</td>)`)
		reg_amount := reg3.FindStringSubmatch(tmpHtml)
		// rs_amount := strings.Replace(reg_amount[0],`+`,``,-1)
		rs_amount := trimHtml(reg_amount[0])

		reg4 := regexp.MustCompile(`(?Us:<td class="status">(.*)</td>)`)
		reg_status := reg4.FindStringSubmatch(tmpHtml)
		rs_status := trimHtml(reg_status[0])

		// fmt.Printf("%+v # %+v # %+v # %+v \n", rs_time,rs_name,rs_amount,rs_status)

		rl = append(rl, RsDataInfo{
			Name:   rs_name,
			Price:  rs_amount,
			Status: rs_status,
			Ts:     rs_time,
		})
	}

	return
}

// //格式化数据
// func formatData(html string) (rl RsList) {
// 	// 创建时间    名称    金额  状态
// 	rl = RsList{}
//
//
// 	reg := regexp.MustCompile(`(?Us:<tr.*>(.*)</tr>)`)
// 	reList := reg.FindAllStringSubmatch(html,-1)
// 	fmt.Printf("正则[所有tr] %+v\n", len(reList))
//
// 	for k, v := range reList {
// 		if k>0{
// 			if len(v)<=0 || v[0]==""{
// 				glog.Errorf("正则数据为空 %+v",v)
// 				return
// 			}
// 			tmpHtml := v[0]
// 			_=gfile.PutContents(fmt.Sprintf("./%v.txt",time.Now().Format("20060102030406")),html)
//
// 			reg1 := regexp.MustCompile(`(?Us:<td class="time">(.*)</td>)`)
// 			reg_time := reg1.FindStringSubmatch(tmpHtml)
// 			if len(reg_time)<=0 || reg_time[0]==""{
// 				glog.Errorf("正则数据[reg_time]为空 %+v",reg_time)
// 				return
// 			}
// 			rs_time := strings.Replace(reg_time[0],`.`,"-",-1)
// 			rs_time = trimHtml(rs_time)+":00"
//
//
// 			reg2 := regexp.MustCompile(`(?Us:<td class="name">(.*)</td>)`)
// 			reg_name := reg2.FindStringSubmatch(tmpHtml)
// 			if len(reg_name)<=0 || reg_name[0]==""{
// 				glog.Errorf("正则数据[reg_name]为空 %+v",reg_name)
// 				return
// 			}
// 			rs_name := trimHtml(reg_name[0])
//
//
// 			reg3 := regexp.MustCompile(`(?Us:<td class="amount">(.*)</td>)`)
// 			reg_amount := reg3.FindStringSubmatch(tmpHtml)
// 			// rs_amount := strings.Replace(reg_amount[0],`+`,``,-1)
// 			if len(reg_amount)<=0 || reg_amount[0]==""{
// 				glog.Errorf("正则数据[reg_amount]为空 %+v",reg_amount)
// 				return
// 			}
// 			rs_amount := trimHtml(reg_amount[0])
//
//
// 			reg4 := regexp.MustCompile(`(?Us:<td class="status">(.*)</td>)`)
// 			reg_status := reg4.FindStringSubmatch(tmpHtml)
// 			if len(reg_status)<=0 || reg_status[0]==""{
// 				glog.Errorf("正则数据[reg_status]为空 %+v",reg_status)
// 				return
// 			}
// 			rs_status:= trimHtml(reg_status[0])
//
// 			// fmt.Println(rs_time,rs_name,rs_amount,rs_status)
//
// 			rl = append(rl,RsDataInfo{
// 				Name:   rs_name,
// 				Price:  rs_amount,
// 				Status: rs_status,
// 				Ts:     rs_time,
// 			})
// 		}
//
// 	}
//
//
//
// 	return
// }

func trimHtml(src string) string {
	// 将HTML标签全转换成小写

	re, _ := regexp.Compile(`\\`)

	src = re.ReplaceAllStringFunc(src, strings.ToLower)

	// 去除STYLE

	re, _ = regexp.Compile(`\\`)

	src = re.ReplaceAllString(src, ``)

	// 去除SCRIPT

	re, _ = regexp.Compile(`\\`)

	src = re.ReplaceAllString(src, ``)

	// 去除所有尖括号内的HTML代码，并换成换行符

	re, _ = regexp.Compile("\\<[\\S\\s]+?\\>")

	src = re.ReplaceAllString(src, " ")

	// 去除连续的换行符

	re, _ = regexp.Compile("\\s{2,}")

	src = re.ReplaceAllString(src, " ")

	// 附加
	re, _ = regexp.Compile(`详情`)
	src = re.ReplaceAllString(src, ``)
	re, _ = regexp.Compile(`备注`)
	src = re.ReplaceAllString(src, ``)
	re, _ = regexp.Compile(`删除`)
	src = re.ReplaceAllString(src, ``)
	return strings.TrimSpace(src)

}
